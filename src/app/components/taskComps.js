const getTaskData = async () => {
  const res = await fetch("http://127.0.0.1:5000/v1/task/all",{
    cache: "no-store"
  });
  return res.json();
};

export default async function ListTasks() {
  const [tasks] = await Promise.all([getTaskData()]);
  return (
    <>
      {tasks.map((task) => {
        return (
          <tbody>
            <tr>
              <td>{task.task_id}</td>
              <td>{task.task_name}</td>
              <td>{task.task_start}</td>
              <td>{task.task_finish}</td>
              <td>{task.task_scope}</td>
              <td>{task.task_status}</td>
              <td><button className="btn btn-ghost btn-xs">details</button></td>
            </tr>
          </tbody>
        );
      })}
    </>
  );
}
