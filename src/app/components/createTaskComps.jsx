import { useState } from "react";
import { useRouter } from "next/navigation";
import axios from "axios";

export default function CreateTaskComps() {
  const router = useRouter();
  const [task_name, settaskName] = useState("");
  const [task_desc, settaskDesc] = useState("");
  const [task_start, settaskStart] = useState("");
  const [task_finish, settaskEnd] = useState("");
  const [task_scope, settaskScope] = useState("");
  const [task_project, settaskProject] = useState("");
  const [task_owner, settaskOwner] = useState("");
  const [task_assign, settaskAssign] = useState("");
  const [task_pic, settaskPIC] = useState("");
  const [task_status, settaskStatus] = useState("");
  const [task_userid, settaskUserid] = useState("SYSTEM");

  async function handleSave() {
    try {
      const res = await fetch("http://127.0.0.1:5000/v1/task/new", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          task_name,
          task_desc,
          task_start,
          task_finish,
          task_scope,
          task_project,
          task_owner,
          task_assign,
          task_pic,
          task_status,
          task_userid,
        }),
      });
      console.log(res);
      if (res.ok) {
        alert("Task saved successfully");
        window.location.href = "/dashboard";
      } else {
        alert(err.message);
        console.log("err", res.statusText);
      }
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <dialog id="createTask" className="modal">
      <form method="dialog" className="modal-box">
        <div className="grid place-items-center">
          <p className="py-4">Task Name</p>
          <input
            type="text"
            className="input input-bordered w-full max-w-xs"
            onChange={(e) => settaskName(e.target.value)}
          />

          <p className="py-4">Task Description</p>
          <textarea
            className="textarea textarea-bordered w-full max-w-xs"
            onChange={(e) => settaskDesc(e.target.value)}
          />

          <p className="py-4">Task Start</p>
          <input
            type="date"
            className="input input-bordered w-full max-w-xs"
            onChange={(e) => settaskStart(e.target.value)}
          />

          <p className="py-4">Task End</p>
          <input
            type="date"
            className="input input-bordered w-full max-w-xs"
            onChange={(e) => settaskEnd(e.target.value)}
          />

          <p className="py-4">Task Scope</p>
          <input
            type="text"
            className="input input-bordered w-full max-w-xs"
            onChange={(e) => settaskScope(e.target.value)}
          />

          <p className="py-4">Task Project</p>
          <input
            type="text"
            className="input input-bordered w-full max-w-xs"
            onChange={(e) => settaskProject(e.target.value)}
          />

          <p className="py-4">Owner</p>
          <input
            type="text"
            className="input input-bordered w-full max-w-xs"
            onChange={(e) => settaskOwner(e.target.value)}
          />

          <p className="py-4">Assign</p>
          <input
            type="text"
            className="input input-bordered w-full max-w-xs"
            onChange={(e) => settaskAssign(e.target.value)}
          />

          <p className="py-4">Task PIC</p>
          <input
            type="text"
            className="input input-bordered w-full max-w-xs"
            onChange={(e) => settaskPIC(e.target.value)}
          />

          <p className="py-4">Status</p>
          <input
            type="text"
            className="input input-bordered w-full max-w-xs"
            onChange={(e) => settaskStatus(e.target.value)}
          />
        </div>

        <div className="modal-action">
          {/* if there is a button in form, it will close the modal */}
          <button className="btn btn-primary" onClick={handleSave}>
            Save
          </button>
          <button className="btn ">Close</button>
        </div>
      </form>
    </dialog>
  );
}
