import Task from "./taskComps";

export default function tableComps() {
  return (
    <div className="bg-primary">
      <div className="overflow-x-auto p-4">
        <table className="table">
          <thead>
            <tr>
              <th>Task ID</th>
              <th>Task Name</th>
              <th>Task Start</th>
              <th>Task Finish</th>
              <th>Task Scope</th>
              <th>Task Status</th>
            </tr>
          </thead>
          <Task />
        </table>
      </div>
    </div>
  );
}
