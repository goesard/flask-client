"use client";

import CreateTask from "./createTaskComps"

export default function filterComps() {
  return (
    <div className="navbar bg-base-300">
      <div className="flex-1"></div>
      <div className="flex-none gap-2">
        <div className="form-control">
          <input
            type="text"
            placeholder="Search"
            className="input input-bordered w-30 h-10 "
          />
        </div>
      </div>

      <button
        className="btn btn-ghost btn-circle avatar"
        onClick={() => window.createTask.showModal()}
      >
        <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="w-6 h-6"
          >
            <path
              fillRule="evenodd"
              d="M12 3.75a.75.75 0 01.75.75v6.75h6.75a.75.75 0 010 1.5h-6.75v6.75a.75.75 0 01-1.5 0v-6.75H4.5a.75.75 0 010-1.5h6.75V4.5a.75.75 0 01.75-.75z"
              clipRule="evenodd"
            />
          </svg>
        </label>
      </button>
      <CreateTask/>
    </div>
  );
}
