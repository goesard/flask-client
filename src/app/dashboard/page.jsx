import Image from "next/image";

// Import Modules
import Filter from "../components/filterComps";
import Table from "../components/tableComps"

export default function DashboardPages() {
  return (
    <>
      <div>
        <div className="drawer lg:drawer-open">
          <input
            id="sidebar-drawer"
            type="checkbox"
            className="drawer-toggle"
          />
          <div className="drawer-content flex flex-col items-center justify-center">
            <div className="grid grid-cols-10 place-items-start py-4">
              <label
                htmlFor="sidebar-drawer"
                className="btn btn-primary drawer-button lg:hidden"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-8 h-8"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                  />
                </svg>
              </label>
            </div>

            <div className="flex flex-col flex-auto p-4 w-full">
              <Filter/>
              <Table/>

              <div className="bg-primary mt-5">03</div>
            </div>
          </div>
          <div className="drawer-side">
            <label htmlFor="sidebar-drawer" className="drawer-overlay"></label>

            <ul className="menu p-4 w-80 h-full bg-base-200 text-base-content">
              <Image
                src="/vercel.svg"
                alt="Vercel Logo"
                className="bg-transparent"
                width={100}
                height={24}
                priority
              />
              <li></li>
              <li>
                <a>Module 1</a>
              </li>
              <li>
                <a>Module 2</a>
              </li>
              <li>
                <a>Module 3</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
}
